CXX=clang++

SRCDIR=src
OBJDIR=bin

DEBUGDIR=debug
RELEASEDIR=release

SRCS=$(wildcard $(SRCDIR)/*.cpp)
OBJS=$(patsubst %.cpp,%.o,$(SRCS))
DEPS=$(patsubst %.o,%.d,$(OBJS))
INCL=-I $(SRCDIR)/

OPTS=-std=c++11 -Wno-c++98-compat -pedantic-errors -MD
CXXFLAGS=$(OPTS)

LFLAGS_D=-Weverything -g -Werror
CFLAGS_D=-Weverything -g -Werror

LFLAGS=-Weverything -Werror
CFLAGS=-Weverything -Werror

LIBS=-lgmpxx -lgmp
LIBS_D=-lgmpxx -lgmp

SHUFI_EXEC_D=$(DEBUGDIR)/shufi_d
SHUFI_EXEC=$(RELEASEDIR)/shufi

debug: $(SHUFI_EXEC_D)

release: $(SHUFI_EXEC)

all: $(SHUFI_EXEC_D) $(SHUFI_EXEC)
	@echo "-- Full compilation completed --"

$(SHUFI_EXEC_D): $(OBJS)
	$(CXX) $(OPTS) $(CFLAGS_D) -o $(SHUFI_EXEC_D) $(SRCS) $(LFLAGS_D) $(LIBS_D)
	@echo "-- Compiled debug --"

$(SHUFI_EXEC): $(OBJS)
	$(CXX) $(OPTS) $(CFLAGS) -o $(SHUFI_EXEC) $(SRCS) $(LFLAGS) $(LIBS)
	@echo "-- Compiled release --"

.PHONY: clean

clean:
	rm -f $(SHUFI_EXEC_D) $(DEBUGDIR)/*.d $(SHUFI_EXEC) $(RELEASEDIR)/*.d $(SRCDIR)/*.o

-include $(DEPS)
