#include <iostream>
#include <vector>
#include <string>

#include <fstream>

#include <assert.h>

#include "Game.h"

using namespace std;

void printHelp();
void storeResults(vector<string> const & participants, vector<string> const & results);

void printHelp(){
    std::cout << "This program takes a list of names from standard input." << std::endl;
}

void storeResults(vector<string> const & participants, vector<string> const & results) {
    ofstream f;
    size_t count = participants.size();
    assert(count == results.size());

    for(size_t i = 0; i < count; i++) {
        f.open(participants[i], ofstream::out | ofstream::trunc);
        f.write(results[i].c_str(), static_cast<streamsize>(results[i].size()));
        f.close();
    }
}

int main(int argc, char* argv[]){
    int nameCount = 0;
    char** names = nullptr;

    if(argc <= 1){
        printHelp();
        return 0;
    }

    nameCount = argc - 1;
    names = &(argv[1]);

    Game myGame(nameCount, names);
    myGame.shuffle();
    auto participants = myGame.participants();
    auto results = myGame.results();
    storeResults(participants, results);

    return 0;
}
