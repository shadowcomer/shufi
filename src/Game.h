#ifndef GAME_H
#define GAME_H

#include <vector>
#include <string>

class Game {
 public:
    Game(int nameCount, char const * const names[]);
    ~Game();

    void shuffle();
    std::vector<std::string> results();
    std::vector<std::string> participants();

 private:
    std::vector<std::string> m_participants;
    std::vector<std::string> m_results;

    bool containsSameMatch();

};

#endif
