#include "Game.h"

#include <iostream>
#include <assert.h>
#include <algorithm>
#include <cstdlib>

Game::Game(int nameCount, char const * const names[]):
    m_participants(std::vector<std::string>(0)),
    m_results(std::vector<std::string>(0)){

    assert(names != nullptr);
    for(int i = 0; i < nameCount; i++) {
        m_participants.push_back(std::string(names[i]));
    }

    srand(static_cast<unsigned int>(time(NULL)));
}

Game::~Game() { }

void Game::shuffle() {
    do {
    m_results = m_participants;
    std::random_shuffle(m_results.begin(),
                        m_results.end());
    } while (containsSameMatch());
}

bool Game::containsSameMatch() {
    size_t count = m_participants.size();
    for(size_t i = 0; i < count; i++) {
        if(m_participants[i] == m_results[i]) {
            return true;
        }
    }

    return false;
}

std::vector<std::string> Game::results() {
    return m_results;
}

std::vector<std::string> Game::participants() {
    return m_participants;
}
